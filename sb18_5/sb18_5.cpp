﻿#include <iostream>
#include <string>
#include <clocale>
// 1. Создайте класс, полями которого будут имя игрока и количество набранных этим игроком очков. 
class Player {
private:
    std::string Nickname;
    int Score;
public:
    Player(std::string _Nickname, int _Score)
    {
        Nickname = _Nickname;
        Score = _Score;
    }
    std::string getNickname() {
        return Nickname;
    }
    int getScore() {
        return Score;
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");
    // 2. Узнайте у пользователя, сколько игроков он хочет добавить, и создайте 
    //    динамический массив необходимого размера. Получите от пользователя имена 
    //    игроков и набранные ими очки и сохраните в массиве.
    int PlayerCount;
    std::cout << "Сколько игроков вы хотите добавить?\n";
    std::cin >> PlayerCount;
    // На самом деле я не особо понимаю что это
    Player **PlayerList = new Player*[PlayerCount];

    std::string Nickname;
    int Score;
    for (int i = 0; i < PlayerCount; i++)
    {
        std::cout << "Введите Никнейм игрока: №" << (i + 1) << ";\n";
        std::cin >> Nickname;
        std::cout << "Введите количество очеков игрока №" << (i + 1) << ":\n";
        std::cin >> Score;
        PlayerList[i] = new Player(Nickname, Score);
    }

    for (int i = 0; i < PlayerCount; i++)
    {
        std::cout << (i + 1) << ". Никнейм: " << PlayerList[i]->getNickname() << "\tКол-во очков - " << PlayerList[i]->getScore() << "\n";
    }

    // 3. Отсортируйте массив по убыванию количества очков, набранных игроками.

    void *tmp = nullptr;

    for (int i = 0; i < PlayerCount; i++)
    {
        for (int j = PlayerCount - 1; j > i; j--)
        {
            if (PlayerList[j]->getScore() > PlayerList[j-1]->getScore())
            {
                // Не знаю запрещен ли swap
                // std::swap(PlayerList[j], PlayerList[j-1]);

                //Вообще не понимаю
                tmp = *&PlayerList[j];
                *&PlayerList[j] = *&PlayerList[j - 1];
                *&PlayerList[j - 1] = static_cast<Player*>(tmp);

            }
        }
    }
    // 4. Выведите все имена и очки игроков в отсортированном виде.
    std::cout << "Отсортировано по убыванию:\n";
    for (int i = 0; i < PlayerCount; i++)
    {
        std::cout << (i + 1) << ". Никнейм: " << PlayerList[i]->getNickname() << "\tКол-во очков - " << PlayerList[i]->getScore() << "\n";
    }

    delete[] PlayerList;
}
